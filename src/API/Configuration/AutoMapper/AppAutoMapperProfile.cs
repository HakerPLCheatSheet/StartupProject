using API.Controllers.Company.Request;
using Application.Companys.Commands.Create;
using Application.Companys.Commands.Update;
using Application.Companys.Queries.Search;
using AutoMapper;

namespace API.Configuration.AutoMapper
{
    public class AppAutoMapperProfile : Profile
    {
        public AppAutoMapperProfile()
        {
            // Request To CQRS
            CreateMap<CreateCompanyRequest, CreateCompanyCommand>();
            CreateMap<UpdateCompanyRequest, UpdateCompanyCommand>();
            CreateMap<SearchCompanyRequest, SearchCompanyQuery>();
        }
    }
}