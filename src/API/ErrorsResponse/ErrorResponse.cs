using System.Collections.Generic;

namespace API.ErrorsResponse
{
    public class ErrorResponse
    {
        public List<ErrorModel> Errors { get; set; } = new List<ErrorModel>();
    }
}