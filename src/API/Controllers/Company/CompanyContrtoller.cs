using System;
using System.Threading.Tasks;
using API.Controllers.Company.Request;
using Application.Companys.Commands.Create;
using Application.Companys.Commands.Delete;
using Application.Companys.Commands.Update;
using Application.Companys.Queries.GetById;
using Application.Companys.Queries.Search;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace API.Controllers.Company
{
    [Route("[controller]/[action]")]
    public class CompanyContrtoller : ApiController
    {
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("{id}", Name = "CompanyGetById")]
        [ProducesResponseType(typeof(GetByIdCompanyQueryResult), 200)] // Success
        [ProducesResponseType(401)] // Unauthorized
        [ProducesResponseType(404)] // Not Found
        [ProducesResponseType(typeof(string), 500)] // Server Error
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                Log.Debug(nameof(GetByIdCompanyQuery));
                var getByIdQueryResult = await Mediator.Send(new GetByIdCompanyQuery(id));

                if (getByIdQueryResult == null)
                {
                    return NotFound();
                }

                return Ok(getByIdQueryResult);
            }
            catch (Exception ex)
            {
                Log.Error($"Something went wrong inside CompanyContrtoller->GetById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost]
        [ProducesResponseType(typeof(int), 200)] // Success
        [ProducesResponseType(401)] // Unauthorized
        [ProducesResponseType(typeof(string), 500)] // Server Error
        public async Task<IActionResult> Create([FromBody] CreateCompanyRequest request)
        {
            try
            {
                Log.Debug(nameof(CreateCompanyRequest));
                var query = Mapper.Map<CreateCompanyCommand>(request);
                var createCompanyQueryResult = await Mediator.Send(query);

                return CreatedAtRoute("CompanyGetById", new { id = createCompanyQueryResult }, createCompanyQueryResult);
            }
            catch (Exception ex)
            {
                Log.Error($"Something went wrong inside CompanyContrtoller->Create action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpDelete("{id}")]
        [ProducesResponseType(200)] // Success
        [ProducesResponseType(401)] // Unauthorized
        [ProducesResponseType(404)] // Not Found
        [ProducesResponseType(typeof(string), 500)] // Server Error
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                Log.Debug(nameof(CreateCompanyRequest));
                var deleteCompanyCommandResult = await Mediator.Send(new DeleteCompanyCommand(id));

                if (!deleteCompanyCommandResult)
                {
                    return NotFound();
                }

                return NoContent();
            }
            catch (Exception ex)
            {
                Log.Error($"Something went wrong inside CompanyContrtoller->Delete action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("{id}")]
        [ProducesResponseType(200)] // Success
        [ProducesResponseType(401)] // Unauthorized
        [ProducesResponseType(404)] // Not Found
        [ProducesResponseType(typeof(string), 500)] // Server Error
        public async Task<IActionResult> Update(int id, [FromBody] UpdateCompanyRequest request)
        {
            try
            {
                Log.Debug(nameof(UpdateCompanyRequest));
                var query = Mapper.Map<UpdateCompanyCommand>(request);
                query.Id = id;
                var updateCompanyCommandResult = await Mediator.Send(query);

                if (!updateCompanyCommandResult)
                {
                    return NotFound();
                }

                return NoContent();
            }
            catch (Exception ex)
            {
                Log.Error($"Something went wrong inside CompanyContrtoller->Update action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost]
        [ProducesResponseType(typeof(SearchCompanyQueryResult), 200)] // Success
        [ProducesResponseType(401)] // Unauthorized
        [ProducesResponseType(404)] // Not Found
        [ProducesResponseType(typeof(string), 500)] // Server Error
        public async Task<IActionResult> Search([FromBody] SearchCompanyRequest request)
        {
            try
            {
                Log.Debug(nameof(SearchCompanyRequest));
                var query = Mapper.Map<SearchCompanyQuery>(request);
                var searchCompanyResponse = await Mediator.Send(query);

                if (searchCompanyResponse.Results == null || searchCompanyResponse.Results.Count == 0)
                {
                    return NotFound();
                }

                return Ok(searchCompanyResponse);
            }
            catch (Exception ex)
            {
                Log.Error($"Something went wrong inside CompanyContrtoller->Search action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}