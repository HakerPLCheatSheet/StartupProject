using API.Controllers.Company.Request;
using FluentValidation;

namespace API.Controllers.Company.Validators
{
    public class CreateCompanyRequestValidator : AbstractValidator<CreateCompanyRequest>
    {
        public CreateCompanyRequestValidator()
        {
            RuleFor(x => x.Name)
                .NotNull()
                .NotEmpty();

            RuleFor(x => x.EstablishmentYear)
                .NotNull();

            RuleFor(x => x.Employees)
                .NotNull();

            RuleForEach(x => x.Employees)
                .Where(x => x != null)
                .SetValidator(new CreateCompanyEmploeesValidator());
        }
    }
}