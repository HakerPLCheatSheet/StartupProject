using Application.Dto;
using FluentValidation;

namespace API.Controllers.Company.Validators
{
    public class CreateCompanyEmploeesValidator : AbstractValidator<EmployeeDto>
    {
        public CreateCompanyEmploeesValidator()
        {
            RuleFor(x => x.FirstName)
                .NotNull()
                .NotEmpty();

            RuleFor(x => x.LastName)
                .NotNull()
                .NotEmpty();

            RuleFor(x => x.DateOfBirth)
                .NotNull();

            RuleFor(x => x.JobTitle)
                .NotNull();
        }
    }
}