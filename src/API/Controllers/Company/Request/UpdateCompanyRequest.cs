using System.Collections.Generic;
using Application.Dto;

namespace API.Controllers.Company.Request
{
    public class UpdateCompanyRequest
    {
        public string Name { get; set; }
        public int EstablishmentYear { get; set; }
        public virtual List<EmployeeDto> Employees { get; set; }
    }
}