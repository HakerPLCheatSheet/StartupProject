using System;
using System.Collections.Generic;

namespace API.Controllers.Company.Request
{
    public class SearchCompanyRequest
    {
        public string Keyword { get; set; }
        public DateTime? EmployeeDateOfBirthFrom { get; set; } = null;
        public DateTime? EmployeeDateOfBirthTo { get; set; } = null;
        public List<string> EmployeeJobTitles { get; set; }
    }
}