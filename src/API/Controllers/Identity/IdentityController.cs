using System;
using System.Threading.Tasks;
using API.Controllers.Identity.Request;
using API.Controllers.Identity.Response;
using Application.Identity.Commands.RefreshToken;
using Application.Identity.Commands.UserLogin;
using Application.Identity.Commands.UserRegistration;
using Domain.Model;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace API.Controllers.Identity
{
    public class IdentityController : ApiController
    {
        [HttpPost]
        [ProducesResponseType(typeof(AuthSuccessResponse), 200)] // Success
        [ProducesResponseType(typeof(AuthFailedResponse), 400)] // Unauthorized
        [ProducesResponseType(typeof(string), 500)] // Server Error
        public async Task<IActionResult> Register([FromBody] UserRegistrationRequest request)
        {
            try
            {
                Log.Debug(nameof(UserRegistrationRequest));
                var authResponse = await Mediator.Send(new UserRegistrationCommand(request.Email, request.Password));
                return ReturnAuthResponse(authResponse);
            }
            catch (Exception ex)
            {
                Log.Error($"Something went wrong inside IdentityController->Register action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        [ProducesResponseType(typeof(AuthSuccessResponse), 200)] // Success
        [ProducesResponseType(typeof(AuthFailedResponse), 400)] // Unauthorized
        [ProducesResponseType(typeof(string), 500)] // Server Error
        public async Task<IActionResult> Login([FromBody] UserLoginRequest request)
        {
            try
            {
                Log.Debug(nameof(UserLoginRequest));
                var authResponse = await Mediator.Send(new UserLoginCommand(request.Email, request.Password));
                return ReturnAuthResponse(authResponse);
            }
            catch (Exception ex)
            {
                Log.Error($"Something went wrong inside IdentityController->Login action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        [ProducesResponseType(typeof(AuthSuccessResponse), 200)] // Success
        [ProducesResponseType(typeof(AuthFailedResponse), 400)] // Unauthorized
        [ProducesResponseType(typeof(string), 500)] // Server Error
        public async Task<IActionResult> Refresh([FromBody] RefreshTokenRequest request)
        {
            try
            {
                Log.Debug(nameof(RefreshTokenRequest));
                var authResponse = await Mediator.Send(new RefreshTokenCommand(request.Token, request.RefreshToken));
                return ReturnAuthResponse(authResponse);
            }
            catch (Exception ex)
            {
                Log.Error($"Something went wrong inside IdentityController->Refresh action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        private IActionResult ReturnAuthResponse(AuthenticationResult authResponse)
        {
            if (!authResponse.Success)
            {
                return BadRequest(new AuthFailedResponse
                {
                    Errors = authResponse.Errors
                });
            }

            return Ok(new AuthSuccessResponse
            {
                Token = authResponse.Token,
                RefreshToken = authResponse.RefreshToken
            });
        }
    }
}