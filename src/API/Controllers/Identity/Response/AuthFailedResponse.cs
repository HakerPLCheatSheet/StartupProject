using System.Collections.Generic;

namespace API.Controllers.Identity.Response
{
    public class AuthFailedResponse
    {
        public IEnumerable<string> Errors { get; set; }
    }
}