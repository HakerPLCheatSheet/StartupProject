using System.ComponentModel.DataAnnotations;

namespace API.Controllers.Identity.Request
{
    public class UserLoginRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}