using System.Diagnostics.CodeAnalysis;
using Domain.Model;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Application.Configuration.Database
{
    public interface IAppDbContext
    {
        DbSet<Company> Companys { get; set; }
        DbSet<Employee> Employees { get; set; }
        DbSet<RefreshToken> RefreshTokens { get; set; }

        DatabaseFacade Database { get; }

        EntityEntry Entry([NotNullAttribute] object entity);
        ValueTask<EntityEntry> AddAsync([NotNullAttribute] object entity, CancellationToken cancellationToken = default);
        ValueTask<EntityEntry<TEntity>> AddAsync<TEntity>([NotNullAttribute] TEntity entity, CancellationToken cancellationToken = default) where TEntity : class;
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken());
        int SaveChanges();
        void Dispose();
    }
}