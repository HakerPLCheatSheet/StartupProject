using Application.Companys.Commands.Create;
using Application.Companys.Commands.Update;
using Application.Companys.Queries.GetById;
using Application.Companys.Queries.Search;
using Application.Dto;
using AutoMapper;
using Domain.Companys;
using Domain.Model;

namespace Application.Configuration.AutoMapper
{
    public class ApplicationAutoMapperProfile : Profile
    {
        public ApplicationAutoMapperProfile()
        {
            // Domain To Dto
            CreateMap<Employee, EmployeeDto>();
            CreateMap<Company, CompanyDto>();


            // Dto To Domain
            CreateMap<EmployeeDto, Employee>();


            // Domain To CQRS
            CreateMap<Company, GetByIdCompanyQueryResult>();


            // CQRS To Domain
            CreateMap<CreateCompanyCommand, Company>();
            CreateMap<UpdateCompanyCommand, Company>();
            CreateMap<SearchCompanyQuery, SearchCompanyFilters>();
        }
    }
}