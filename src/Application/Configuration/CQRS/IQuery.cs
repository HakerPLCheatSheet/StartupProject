using MediatR;

namespace Application.Configuration.CQRS
{
    public interface IQuery<out TResult> : IRequest<TResult>
    {

    }
}