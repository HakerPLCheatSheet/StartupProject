using MediatR;

namespace Application.Configuration.CQRS
{
    public interface ICommand<out TResult> : IRequest<TResult>
    {

    }

    public interface ICommand : IRequest
    {

    }
}