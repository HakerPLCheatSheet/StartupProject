using MediatR;

namespace Application.Configuration.CQRS
{
    public interface IQueryHandler<in IQuery, TResult> :
            IRequestHandler<IQuery, TResult> where IQuery : IQuery<TResult>
    {

    }
}
