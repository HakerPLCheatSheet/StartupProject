using System.Collections.Generic;

namespace Application.Dto
{
    public class CompanyDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int EstablishmentYear { get; set; }
        public List<EmployeeDto> Employees { get; set; }
    }
}