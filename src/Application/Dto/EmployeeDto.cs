using System;
using Domain.Model;

namespace Application.Dto
{
    public class EmployeeDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public EmployeePosition JobTitle { get; set; }
    }
}