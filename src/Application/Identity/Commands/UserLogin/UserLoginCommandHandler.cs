using System.Threading;
using System.Threading.Tasks;
using Application.Configuration.CQRS;
using Application.Configuration.Database;
using Domain.Model;
using Microsoft.AspNetCore.Identity;

namespace Application.Identity.Commands.UserLogin
{
    public class UserLoginCommandHandler : IdentitysBaseHandler, IQueryHandler<UserLoginCommand, AuthenticationResult>
    {
        public UserLoginCommandHandler(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager, JwtSettings jwtSettings, IAppDbContext dbContext)
                : base(userManager, roleManager, jwtSettings, dbContext)
        {
        }

        public async Task<AuthenticationResult> Handle(UserLoginCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);

            if (user == null)
            {
                return new AuthenticationResult
                {
                    Errors = new[] { "User does not exists" }
                };
            }

            var userHasValidPassword = await _userManager.CheckPasswordAsync(user, request.Password);

            if (!userHasValidPassword)
            {
                return new AuthenticationResult
                {
                    Errors = new[] { "User/password combination is wrong" }
                };
            }

            return await GenerateAuthenticationResultForUserAsync(user);
        }
    }
}