using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Application.Configuration.CQRS;
using Application.Configuration.Database;
using Domain.Model;
using Microsoft.AspNetCore.Identity;

namespace Application.Identity.Commands.UserRegistration
{
    public class UserRegistrationCommandHandler : IdentitysBaseHandler, IQueryHandler<UserRegistrationCommand, AuthenticationResult>
    {
        public UserRegistrationCommandHandler(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager, JwtSettings jwtSettings, IAppDbContext dbContext)
                : base(userManager, roleManager, jwtSettings, dbContext)
        {
        }

        public async Task<AuthenticationResult> Handle(UserRegistrationCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);

            if (user != null)
            {
                return new AuthenticationResult
                {
                    Errors = new[] { "User with this email address already exists" }
                };
            }

            var newUserId = Guid.NewGuid();

            var newUser = new IdentityUser
            {
                Id = newUserId.ToString(),
                Email = request.Email,
                UserName = request.Email
            };

            var createdUser = await _userManager.CreateAsync(newUser, request.Password);

            if (!createdUser.Succeeded)
            {
                return new AuthenticationResult
                {
                    Errors = createdUser.Errors.Select(x => x.Description)
                };
            }

            //await _userManager.AddClaimAsync(newUser, new Claim("tags.view", "true"));

            return await GenerateAuthenticationResultForUserAsync(newUser);
        }
    }
}