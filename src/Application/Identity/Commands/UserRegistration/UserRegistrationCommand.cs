using Application.Configuration.CQRS;
using Domain.Model;

namespace Application.Identity.Commands.UserRegistration
{
    public class UserRegistrationCommand : IQuery<AuthenticationResult>
    {
        public string Email { get; }
        public string Password { get; }

        public UserRegistrationCommand(string email, string password)
        {
            Email = email;
            Password = password;
        }
    }
}