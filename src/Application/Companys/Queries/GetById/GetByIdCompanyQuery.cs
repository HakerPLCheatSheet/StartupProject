using Application.Configuration.CQRS;

namespace Application.Companys.Queries.GetById
{
    public class GetByIdCompanyQuery : IQuery<GetByIdCompanyQueryResult>
    {
        public int CompanyId { get; }

        public GetByIdCompanyQuery(int companyId)
        {
            CompanyId = companyId;
        }
    }
}