using System.Collections.Generic;
using Application.Dto;

namespace Application.Companys.Queries.GetById
{
    public class GetByIdCompanyQueryResult
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int EstablishmentYear { get; set; }
        public List<EmployeeDto> Employees { get; set; }
    }
}