using System.Threading;
using System.Threading.Tasks;
using Application.Configuration.CQRS;
using Application.Configuration.Database;
using AutoMapper;
using Domain.Model;

namespace Application.Companys.Queries.GetById
{
    public class GetByIdCompanyQueryHandler : CompanysBaseHandler, IQueryHandler<GetByIdCompanyQuery, GetByIdCompanyQueryResult>
    {
        public GetByIdCompanyQueryHandler(IAppDbContext dbContext, IMapper mapper) : base(dbContext, mapper) { }

        public async Task<GetByIdCompanyQueryResult> Handle(GetByIdCompanyQuery request, CancellationToken cancellationToken)
        {
            Company company = await CompanyGetById(request.CompanyId);
            return _mapper.Map<GetByIdCompanyQueryResult>(company);
        }
    }
}