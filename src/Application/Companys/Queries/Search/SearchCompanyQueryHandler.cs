using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Configuration.CQRS;
using Application.Configuration.Database;
using Application.Dto;
using AutoMapper;
using Domain.Companys;
using Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace Application.Companys.Queries.Search
{
    public class SearchCompanyQueryHandler : IQueryHandler<SearchCompanyQuery, SearchCompanyQueryResult>
    {
        private IAppDbContext _dbContext;
        private IMapper _mapper;

        public SearchCompanyQueryHandler(IAppDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<SearchCompanyQueryResult> Handle(SearchCompanyQuery request, CancellationToken cancellationToken)
        {
            var queryable = _dbContext.Companys
                                    .Include(x => x.Employees)
                                    .AsQueryable();

            var filter = _mapper.Map<SearchCompanyFilters>(request);
            List<Company> companys;

            if (filter != null && CompanyFilter.FilterParamsValidation(filter))
            {
                companys = CompanyFilter.AddFiltersOnQuery(filter, queryable);
            }
            else
            {
                companys = await queryable.ToListAsync();
            }

            return new SearchCompanyQueryResult { Results = _mapper.Map<List<CompanyDto>>(companys) };
        }
    }
}