using System;
using System.Collections.Generic;
using Application.Configuration.CQRS;

namespace Application.Companys.Queries.Search
{
    public class SearchCompanyQuery : IQuery<SearchCompanyQueryResult>
    {
        public string Keyword { get; set; }
        public DateTime? EmployeeDateOfBirthFrom { get; set; } = null;
        public DateTime? EmployeeDateOfBirthTo { get; set; } = null;
        public List<string> EmployeeJobTitles { get; set; }
    }
}