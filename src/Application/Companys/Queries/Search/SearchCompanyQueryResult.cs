using System.Collections.Generic;
using Application.Dto;

namespace Application.Companys.Queries.Search
{
    public class SearchCompanyQueryResult
    {
        public List<CompanyDto> Results { get; set; }
    }
}