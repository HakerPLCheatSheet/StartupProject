using Application.Configuration.CQRS;

namespace Application.Companys.Commands.Delete
{
    public class DeleteCompanyCommand : IQuery<bool>
    {
        public int CompanyId { get; }

        public DeleteCompanyCommand(int companyId)
        {
            CompanyId = companyId;
        }
    }
}