using System.Threading;
using System.Threading.Tasks;
using Application.Configuration.CQRS;
using Application.Configuration.Database;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Application.Companys.Commands.Delete
{
    public class DeleteCompanyCommandHandler : CompanysBaseHandler, IQueryHandler<DeleteCompanyCommand, bool>
    {
        public DeleteCompanyCommandHandler(IAppDbContext dbContext, IMapper mapper) : base(dbContext, mapper) { }

        public async Task<bool> Handle(DeleteCompanyCommand request, CancellationToken cancellationToken)
        {
            var company = await CompanyGetById(request.CompanyId);

            if (company == null)
            {
                return false;
            }

            _dbContext.Companys.Remove(company);
            var deleted = await _dbContext.SaveChangesAsync();

            return deleted > 0;
        }
    }
}