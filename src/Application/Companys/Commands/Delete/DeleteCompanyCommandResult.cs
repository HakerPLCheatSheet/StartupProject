namespace Application.Companys.Commands.Delete
{
    public class DeleteCompanyCommandResult
    {
        public bool Deleted { get; set; }
    }
}