using System.Threading;
using System.Threading.Tasks;
using Application.Configuration.CQRS;
using Application.Configuration.Database;
using AutoMapper;
using Domain.Model;

namespace Application.Companys.Commands.Update
{
    public class UpdateCompanyCommandHandler : CompanysBaseHandler, IQueryHandler<UpdateCompanyCommand, bool>
    {
        public UpdateCompanyCommandHandler(IAppDbContext dbContext, IMapper mapper) : base(dbContext, mapper) { }

        public async Task<bool> Handle(UpdateCompanyCommand request, CancellationToken cancellationToken)
        {
            var company = await CompanyGetById(request.Id);

            if (company == null)
            {
                return false;
            }

            var updateCompany = _mapper.Map<Company>(request);
            _dbContext.Companys.Update(updateCompany);
            var updated = await _dbContext.SaveChangesAsync();

            return updated > 0;
        }
    }
}