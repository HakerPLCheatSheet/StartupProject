using System.Collections.Generic;
using Application.Configuration.CQRS;
using Application.Dto;

namespace Application.Companys.Commands.Update
{
    public class UpdateCompanyCommand : IQuery<bool>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int EstablishmentYear { get; set; }
        public List<EmployeeDto> Employees { get; set; }
    }
}