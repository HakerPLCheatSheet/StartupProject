using System.Collections.Generic;
using Application.Configuration.CQRS;
using Application.Dto;

namespace Application.Companys.Commands.Create
{
    public class CreateCompanyCommand : IQuery<int>
    {
        public string Name { get; set; }
        public int EstablishmentYear { get; set; }
        public List<EmployeeDto> Employees { get; set; }
    }
}