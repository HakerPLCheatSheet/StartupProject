using System.Threading;
using System.Threading.Tasks;
using Application.Configuration.CQRS;
using Application.Configuration.Database;
using AutoMapper;
using Domain.Model;

namespace Application.Companys.Commands.Create
{
    public class CreateCompanyCommandHandler : IQueryHandler<CreateCompanyCommand, int>
    {
        private IAppDbContext _dbContext;
        private IMapper _mapper;

        public CreateCompanyCommandHandler(IAppDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<int> Handle(CreateCompanyCommand request, CancellationToken cancellationToken)
        {
            var newCompany = _mapper.Map<Company>(request);
            await _dbContext.Companys.AddAsync(newCompany);
            await _dbContext.SaveChangesAsync();

            return newCompany.Id;
        }
    }
}