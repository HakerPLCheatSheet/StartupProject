using System.Threading.Tasks;
using Application.Configuration.Database;
using AutoMapper;
using Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace Application.Companys
{
    public abstract class CompanysBaseHandler
    {
        protected IAppDbContext _dbContext;
        protected IMapper _mapper;


        public CompanysBaseHandler(IAppDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        protected async Task<Company> CompanyGetById(int id)
        {
            return await _dbContext.Companys
                            .AsNoTracking()
                            .Include(x => x.Employees)
                            .SingleOrDefaultAsync(x => x.Id == id);
        }
    }
}