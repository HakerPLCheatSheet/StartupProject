using Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Database.Configuration
{
    public class RefreshTokenConfiguration : IEntityTypeConfiguration<RefreshToken>
    {
        public void Configure(EntityTypeBuilder<RefreshToken> builder)
        {
            builder.HasKey(r => r.Token);

            builder.Property(r => r.Token)
                .ValueGeneratedOnAdd();

            builder.Property(r => r.CreationDate)
                .HasDefaultValueSql("getdate()");
        }
    }
}