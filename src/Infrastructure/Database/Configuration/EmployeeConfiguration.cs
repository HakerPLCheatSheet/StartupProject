using System;
using Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Database.Configuration
{
    public class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.Property(t => t.FirstName)
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(t => t.LastName)
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(e => e.JobTitle)
                    .HasConversion(x => x.ToString(), // to converter
                        x => (EmployeePosition)Enum.Parse(typeof(EmployeePosition), x));// from converter

            builder.HasOne<Company>(e => e.Company)
                .WithMany(d => d.Employees)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}