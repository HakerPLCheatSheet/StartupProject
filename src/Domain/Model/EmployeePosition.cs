using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Domain.Model
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum EmployeePosition
    {
        Administrator,
        Developer,
        Architect,
        Manager
    }
}