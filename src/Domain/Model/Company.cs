using System.Collections.Generic;

namespace Domain.Model
{
    public class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int EstablishmentYear { get; set; }
        public virtual List<Employee> Employees { get; set; }
    }
}