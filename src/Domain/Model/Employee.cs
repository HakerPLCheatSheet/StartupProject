using System;

namespace Domain.Model
{
    public class Employee
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public EmployeePosition JobTitle { get; set; }
        public virtual Company Company { get; set; }
    }
}