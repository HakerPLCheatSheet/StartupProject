using System;
using System.Collections.Generic;

namespace Domain.Companys
{
    public class SearchCompanyFilters
    {
        public string Keyword { get; set; }
        public DateTime? EmployeeDateOfBirthFrom { get; set; } = null;
        public DateTime? EmployeeDateOfBirthTo { get; set; } = null;
        public List<string> EmployeeJobTitles { get; set; }
    }
}