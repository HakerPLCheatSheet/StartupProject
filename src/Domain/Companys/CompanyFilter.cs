using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Model;

namespace Domain.Companys
{
    public static class CompanyFilter
    {
        public static bool FilterParamsValidation(SearchCompanyFilters filter)
        {
            return !string.IsNullOrEmpty(filter.Keyword) ||
                        (filter.EmployeeDateOfBirthFrom.HasValue && filter.EmployeeDateOfBirthTo.HasValue) ||
                        (filter.EmployeeJobTitles != null && filter.EmployeeJobTitles.Count > 0);
        }

        public static List<Company> AddFiltersOnQuery(SearchCompanyFilters filter, IQueryable<Company> queryable)
        {
            List<Company> companys = new List<Company>();

            try
            {
                var predicate = PredicateBuilder.False<Company>();

                if (!string.IsNullOrEmpty(filter.Keyword))
                {
                    predicate = predicate.Or(company => company.Name.Contains(filter.Keyword));
                    predicate = predicate.Or(company => company.Employees
                                        .Where(emploee => emploee.FirstName.Contains(filter.Keyword) ||
                                                          emploee.LastName.Contains(filter.Keyword)).Any());
                }

                if (filter.EmployeeDateOfBirthFrom.HasValue && filter.EmployeeDateOfBirthTo.HasValue)
                {
                    predicate = predicate.Or(company => company.Employees.Where(emploee => emploee.DateOfBirth.Date >= filter.EmployeeDateOfBirthFrom.Value.Date &&
                                                emploee.DateOfBirth.Date <= filter.EmployeeDateOfBirthTo.Value.Date).Any());

                }

                if (filter.EmployeeJobTitles != null && filter.EmployeeJobTitles.Count > 0)
                {
                    foreach (string jobTitle in filter.EmployeeJobTitles)
                    {
                        if (Enum.TryParse(jobTitle, out EmployeePosition jobTitleEnum))
                        {
                            predicate = predicate.Or(company => company.Employees.Where(emploee => emploee.JobTitle == jobTitleEnum).Any());
                        }
                    }
                }

                companys = queryable.Where(predicate).DistinctBy(x => x.Id).ToList();
            }
            catch (Exception ex)
            {
                companys = queryable.ToList();
            }

            return companys;
        }
    }
}